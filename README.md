# twitch-curses

This is a simple twitch.tv browser / livestreamer frontend made with python and ncurses that I hacked together one day. Maybe someone will find it useful.

The controls should be pretty straightforward.

## License

[WTFPL](https://gitlab.com/corbie/twitch-curses/blob/master/LICENSE)